module.exports = {
    verbose: true,
    setupFilesAfterEnv: ["jest-allure/dist/setup"],
    "testRunner": "jest-jasmine2"
}