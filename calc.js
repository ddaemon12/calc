console.log('Greetings! Choose the action you want me to do\n')
console.log('Type sum(), diff(), mult() or quot()')
console.log()

function sum(...nums){
    let res = 0
    for (const num in nums) {
        res += nums[num];
    }
    if (res === -0){res = 0}
    console.log(`Sum of nums is ${res}`)
    return res
}

function diff(...nums){
    for (let i=1; i<nums.length; i++){
        nums[0] -= nums[i]
    }
    if (nums[0] === -0){nums[0] = 0}
    console.log(`Difference of nums is ${nums[0]}`)
    return nums[0]
}

function mult(...nums){
    let res = 1
    for (const num in nums){
        res = res*nums[num]
    }
    if (res === -0){res = 0}
    console.log(`Product is ${res}`)
    return res
}

function quot(num1, num2){
    if(num2 != 0){
        res = num1 / num2
        console.log(`Quotient is ${res}`)
        if (res === -0){res = 0}
        return res
    }else{
        console.log("I can`t divide by zero")
        return "I can`t divide by zero"
    }
}

module.exports = {sum, diff, quot, mult}